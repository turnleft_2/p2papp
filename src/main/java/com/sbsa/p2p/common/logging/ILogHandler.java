package com.sbsa.p2p.common.logging;

public interface ILogHandler {
	/**
	 * Logs messages with level "DEBUG".
	 * @param msg message to log
	 * 
	 */
	public void logMessage(Object msg);

	/**
	 * Logs messages with level "TRACE".
	 * @param msg message to log
	 * 
	 */
	public void logTrace(Object msg);
	
	/**
	 * Logs messages with level "INFO".
	 * @param msg message to log
	 * 
	 */
	public void logInfo(Object msg);
	
	/**
	 * Logs messages with level "WARN".
	 * @param msg message to log
	 * 
	 */
	public void logWarn(Object msg);
	
	/**
	 * Logs messages with level "ERROR".
	 * @param msg message to log
	 * 
	 */
	public void logError(Object msg);
	
	/**
	 * Logs messages with level "FATAL".
	 * @param msg message to log
	 * 
	 */
    public void logFatal(Object msg);
    
	/**
	 * Logs messages with level "ERROR".
	 * @param msg message to log
	 * @param t error object to print error stack
	 * 
	 */
    public void logError(Object msg, Throwable t);

}
