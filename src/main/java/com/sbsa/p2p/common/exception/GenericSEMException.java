package com.sbsa.p2p.common.exception;

public class GenericSEMException extends RuntimeException  {

	private static final long serialVersionUID = 4753119999991684843L;
	private Throwable throwable;
	public Throwable getThrowable() {
		return throwable;
	}

	public void setThrowable(Throwable throwable) {
		this.throwable = throwable;
	}

	private String errCode;
	private String errMsg;
 

	public GenericSEMException(String errCode, String errMsg, Throwable ex) {
		this.throwable = ex;
		this.setErrCode(errCode);
		this.setErrMsg(errMsg);
	}

	public String getErrCode() {
		return errCode;
	}

	public void setErrCode(String errCode) {
		this.errCode = errCode;
	}

	public String getErrMsg() {
		return errMsg;
	}

	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}


}
