package com.sbsa.p2p.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "USER_CREDS", uniqueConstraints = { @UniqueConstraint(columnNames = "USER_CRED_ID") })
public class UserCreds {

	
	@SequenceGenerator(name="usercred_seq_gen", sequenceName="USER_CRED_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="usercred_seq_gen") 
	@Column(name="USER_CRED_ID", nullable = false) 
	private Long userCredId;
		
    @ManyToOne
	@JoinColumn(name="PERSON_ID")
	private Person  user;
    
    @Column(name="USER_NAME")
    private String userName;
    
    @Column(name="USER_CRED")
    private String userCred;

	public Long getUserCredId() {
		return userCredId;
	}

	public void setUserCredId(Long userCredId) {
		this.userCredId = userCredId;
	}

	public Person getUser() {
		return user;
	}

	public void setUser(Person user) {
		this.user = user;
	}

	public String getUserName() {
		return userName;
	}

	public void setUserName(String userName) {
		this.userName = userName;
	}

	public String getUserCred() {
		return userCred;
	}

	public void setUserCred(String userCred) {
		this.userCred = userCred;
	}


}
