package com.sbsa.p2p.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "BORROW_REQ", uniqueConstraints = { @UniqueConstraint(columnNames = "BORROW_REQ_ID") })
public class BorrowerRequest implements java.io.Serializable {
	
	@SequenceGenerator(name="borrowReq_seq_gen", sequenceName="BORROW_REQ_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="borrowReq_seq_gen") 
	@Column(name="BORROW_REQ_ID", nullable = false) 
	private Long borrowReqId;
	
	
    @ManyToOne
	@JoinColumn(name="PERSON_ID")
	private Person  person;
    
    @Column(name="AMT_TO_BORROW")
    private double borrowAmt;
    
    @Column(name="RATE_REQUESTED")
    private double rateRequested;
    
    @Column(name="TENURE_REQUESTED")
    private long tenureRequested;
    
    @Column(name="STATUS")
    private String status;

	public Long getBorrowReqId() {
		return borrowReqId;
	}

	public void setBorrowReqId(Long borrowReqId) {
		this.borrowReqId = borrowReqId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public double getBorrowAmt() {
		return borrowAmt;
	}

	public void setBorrowAmt(double borrowAmt) {
		this.borrowAmt = borrowAmt;
	}

	public double getRateRequested() {
		return rateRequested;
	}

	public void setRateRequested(double rateRequested) {
		this.rateRequested = rateRequested;
	}

	public long getTenureRequested() {
		return tenureRequested;
	}

	public void setTenureRequested(long tenureRequested) {
		this.tenureRequested = tenureRequested;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
    
    

}
