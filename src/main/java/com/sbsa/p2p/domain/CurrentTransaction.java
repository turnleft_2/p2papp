package com.sbsa.p2p.domain;

import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "CURRENT_TRANSACTION", uniqueConstraints = { @UniqueConstraint(columnNames = "CURR_TRANS_ID") })
public class CurrentTransaction implements java.io.Serializable  {


		private static final long serialVersionUID = 7257386315331273385L;

		@SequenceGenerator(name="currTrans_seq_gen", sequenceName="CURR_TRANS_SEQ") 	
		@Id
		@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="currTrans_seq_gen") 
		@Column(name="CURR_TRANS_ID", nullable = false) 
		private Long currTransId;
	
	    @ManyToOne 
		@JoinColumn(name="PERSON_ID")
		private Person  lender;
	    
	    @ManyToOne
		@JoinColumn(name="PERSON_ID",insertable = false, updatable = false)
		private Person  borrower;

	    @Column(name="AMOUNT")
	    private long amount;
	    
	    @Column(name="START_DATE")
	    private Date startDate;
	    
	    @Column(name="DUE_DATE")
	    private Date dueDt;
	    
	    @Column(name="ACTUAL_PAYDATE")
	    private Date actPayDt;
	    
	    @Column(name="BORROWER_RATE")
	    private Date borrowerRate;

	    @Column(name="STATUS")
	    private Date status;
		
	    
	    @ManyToOne
		@JoinColumn(name="RULE_ID")
	    private RuleSet  ruleSet;

		
}
