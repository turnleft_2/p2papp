package com.sbsa.p2p.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "SUGGESTION_DETAILS", uniqueConstraints = { @UniqueConstraint(columnNames = "SUGG_ID") })
public class SuggestionDetails {
	
	@SequenceGenerator(name="sugg_dtl_seq_gen", sequenceName="SUGG_DTL_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sugg_dtl_seq_gen") 
	@Column(name="SUGG_DTL_ID", nullable = false) 
	private Long suggDtlId;
	
    @ManyToOne
	@JoinColumn(name="SUGG_ID")
	private Suggestions suggestions;
	
    @ManyToOne
	@JoinColumn(name="PERSON_ID")
	private Person  lenderSugg;

    
    @Column(name="AMOUNT")
    private double amount;


	public Long getSuggDtlId() {
		return suggDtlId;
	}


	public void setSuggDtlId(Long suggDtlId) {
		this.suggDtlId = suggDtlId;
	}


	public Suggestions getSuggestions() {
		return suggestions;
	}


	public void setSuggestions(Suggestions suggestions) {
		this.suggestions = suggestions;
	}


	public Person getLenderSugg() {
		return lenderSugg;
	}


	public void setLenderSugg(Person lenderSugg) {
		this.lenderSugg = lenderSugg;
	}


	public double getAmount() {
		return amount;
	}


	public void setAmount(double amount) {
		this.amount = amount;
	}
    
    

}
