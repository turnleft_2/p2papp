package com.sbsa.p2p.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;


@Entity
@Table(name = "RULESET", uniqueConstraints = { @UniqueConstraint(columnNames = "RULESET_ID") })
public class RuleSet  implements java.io.Serializable {

	@SequenceGenerator(name="ruleSet_seq_gen", sequenceName="RULESET_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="ruleSet_seq_gen") 
	@Column(name="RULESET_ID", nullable = false) 
	private Long ruleSetId;
	
    @Column(name="MAX_AMT")
    private double maxAmt;
	
    @Column(name="INTEREST_RATE")
    private double interestRate;
	
    @Column(name="BANK_FEE_RATE")
    private double bankFeeRate;
    
    @Column(name="LNDR_COUNT")
    private int lenderCount;

    @Column(name="RISK_RATING")
    private String riskRating;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "ruleSet", cascade = { CascadeType.ALL })
	private Set<CurrentTransaction> currentTransList = new HashSet<CurrentTransaction>(0);

	public Long getRuleSetId() {
		return ruleSetId;
	}

	public void setRuleSetId(Long ruleSetId) {
		this.ruleSetId = ruleSetId;
	}

	public double getMaxAmt() {
		return maxAmt;
	}

	public void setMaxAmt(double maxAmt) {
		this.maxAmt = maxAmt;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public double getBankFeeRate() {
		return bankFeeRate;
	}

	public void setBankFeeRate(double bankFeeRate) {
		this.bankFeeRate = bankFeeRate;
	}

	public Set<CurrentTransaction> getCurrentTransList() {
		return currentTransList;
	}

	public void setCurrentTransList(Set<CurrentTransaction> currentTransList) {
		this.currentTransList = currentTransList;
	}

	public int getLenderCount() {
		return lenderCount;
	}

	public void setLenderCount(int lenderCount) {
		this.lenderCount = lenderCount;
	}

	public String getRating() {
		return riskRating;
	}

	public void setRating(String rating) {
		this.riskRating = rating;
	}


}


