package com.sbsa.p2p.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
	
@Entity
@Table(name = "LENDER_PROF", uniqueConstraints = { @UniqueConstraint(columnNames = "LNDR_PROF_ID") })
public class LenderProfile implements java.io.Serializable {

	private static final long serialVersionUID = 7257386315181273385L;

	@SequenceGenerator(name="lenderProf_seq_gen", sequenceName="LENDER_PROF_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="lenderProf_seq_gen") 
	@Column(name="LNDR_PROF_ID", nullable = false) 
	private Long lndrProfId;
	    
    @ManyToOne
	@JoinColumn(name="PERSON_ID")
	private Person  person;
    
    @Column(name="AMT_TO_LEND")
    private double lendAmt;
    
    // in %
    @Column(name="RATE_REQUIRED")
    private double rateRequired;

    //in days
    @Column(name="TENURE_REQUIRED")
    private int tenureRequired;
    
    // as rating
    @Column(name="RISK_REQUIRED")
    private String riskRequired;
	
    @Column(name="ACTIVE")
    private String isActive;
	
    @Column(name="CURR_BAL")
    private double currentBalance;

    @Column(name="SUG_BAL")
    private double suggBal;

	public Long getLndrProfId() {
		return lndrProfId;
	}

	public void setLndrProfId(Long lndrProfId) {
		this.lndrProfId = lndrProfId;
	}

	public Person getPerson() {
		return person;
	}

	public void setPerson(Person person) {
		this.person = person;
	}

	public double getLendAmt() {
		return lendAmt;
	}

	public void setLendAmt(double lendAmt) {
		this.lendAmt = lendAmt;
	}


	public double getRateRequired() {
		return rateRequired;
	}

	public void setRateRequired(double rateRequired) {
		this.rateRequired = rateRequired;
	}


	public int getTenureRequired() {
		return tenureRequired;
	}

	public void setTenureRequired(int tenureRequired) {
		this.tenureRequired = tenureRequired;
	}

	public String getRiskRequired() {
		return riskRequired;
	}

	public void setRiskRequired(String riskRequired) {
		this.riskRequired = riskRequired;
	}

	public String getIsActive() {
		return isActive;
	}

	public void setIsActive(String isActive) {
		this.isActive = isActive;
	}

	public double getCurrentBalance() {
		return currentBalance;
	}

	public void setCurrentBalance(double currentBalance) {
		this.currentBalance = currentBalance;
	}

	public double getSuggBal() {
		return suggBal;
	}

	public void setSuggBal(double suggBal) {
		this.suggBal = suggBal;
	}

    
}
