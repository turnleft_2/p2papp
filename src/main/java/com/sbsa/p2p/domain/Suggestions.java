package com.sbsa.p2p.domain;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;

@Entity
@Table(name = "SUGGESTIONS", uniqueConstraints = { @UniqueConstraint(columnNames = "SUGG_ID") })
public class Suggestions  implements java.io.Serializable {

	@SequenceGenerator(name="sugg_seq_gen", sequenceName="SUGG_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="sugg_seq_gen") 
	@Column(name="SUGG_ID", nullable = false) 
	private Long suggId;
	
    @Column(name="AMOUNT")
    private double amount;
	
    @Column(name="INTEREST_RATE")
    private double interestRate;
    
    @Column(name="TENURE")
    private long tenure;

	public Long getSuggId() {
		return suggId;
	}

	public void setSuggId(Long suggId) {
		this.suggId = suggId;
	}

	public double getAmount() {
		return amount;
	}

	public void setAmount(double amount) {
		this.amount = amount;
	}

	public double getInterestRate() {
		return interestRate;
	}

	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}

	public long getTenure() {
		return tenure;
	}

	public void setTenure(long tenure) {
		this.tenure = tenure;
	}

	
}
