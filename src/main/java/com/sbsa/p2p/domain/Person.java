package com.sbsa.p2p.domain;

import java.util.Date;
import java.util.HashSet;
import java.util.Set;

import javax.persistence.CascadeType;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;



@Entity
@Table(name = "PERSON", uniqueConstraints = { @UniqueConstraint(columnNames = "PERSON_ID") })
public class Person implements java.io.Serializable {

	private static final long serialVersionUID = 7257386315181273385L;

	@SequenceGenerator(name="person_seq_gen", sequenceName="PERSON_SEQ") 	
	@Id
	@GeneratedValue(strategy=GenerationType.SEQUENCE, generator="person_seq_gen") 
	@Column(name="PERSON_ID", nullable = false) 
	private Long personId;
    
    
    @Column(name="FIRST_NAME") 
    private String firstNm;

    @Column(name="LAST_NAME")
    private String lastNm;
    
    @Column(name="UNIQ_ID")
    private String uniqID;
    
    @Column(name="UNIQ_ID_TYPE")
    private String uniqIdName;
    
    @Column(name="PERSON_TYPE")
    private String personTyp;
    
    @Column(name="ACCNT_NUM")
    private String accNum;
    
    @Column(name="ACCNT_BNK_NM")
    private String accntBnkNm;
    
    @Column(name="ACCNT_BNK_BRNCH_CDE")
    private String accntBnkBrnchCde;
    
    @Column(name="ACCNT_BNK_BRNCH_BIC")
    private String accntBnkBrnchBic;
    
    @Column(name="PHONE_NUM")
    private String phoneNum;
    
    @Column(name="ADDR_LINE1")
    private String addrLine1;
    
    @Column(name="ADDR_LINE2")
    private String addrLine2;
    
    @Column(name="RISK_RATING")
    private String riskRating;
    
    @Column(name="REGISTER_DT")
    private Date regDt;
    
    @Column(name="PROF_UPDATE_DT")
    private Date profUptDt;
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "person", cascade = { CascadeType.ALL })
	private Set<BorrowerRequest> borrowerReqList = new HashSet<BorrowerRequest>(0);
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "lender", cascade = { CascadeType.ALL })
	private Set<CurrentTransaction> lenderTransList = new HashSet<CurrentTransaction>(0);
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "borrower", cascade = { CascadeType.ALL })
	private Set<CurrentTransaction> borrowerTransList = new HashSet<CurrentTransaction>(0);
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "lenderSugg", cascade = { CascadeType.ALL })
	private Set<SuggestionDetails> lenderSuggList = new HashSet<SuggestionDetails>(0);
    
    @OneToMany(fetch = FetchType.EAGER, mappedBy = "user", cascade = { CascadeType.ALL })
	private Set<UserCreds> userList = new HashSet<UserCreds>(0);

	public Long getPersonId() {
		return personId;
	}

	public void setPersonId(Long personId) {
		this.personId = personId;
	}

	public String getFirstNm() {
		return firstNm;
	}

	public void setFirstNm(String firstNm) {
		this.firstNm = firstNm;
	}

	public String getLastNm() {
		return lastNm;
	}

	public void setLastNm(String lastNm) {
		this.lastNm = lastNm;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getUniqID() {
		return uniqID;
	}

	public void setUniqID(String uniqID) {
		this.uniqID = uniqID;
	}

	public String getUniqIdName() {
		return uniqIdName;
	}

	public void setUniqIdName(String uniqIdName) {
		this.uniqIdName = uniqIdName;
	}

	public String getPersonTyp() {
		return personTyp;
	}

	public void setPersonTyp(String personTyp) {
		this.personTyp = personTyp;
	}

	public String getAccNum() {
		return accNum;
	}

	public void setAccNum(String accNum) {
		this.accNum = accNum;
	}

	public String getAccntBnkNm() {
		return accntBnkNm;
	}

	public void setAccntBnkNm(String accntBnkNm) {
		this.accntBnkNm = accntBnkNm;
	}

	public String getAccntBnkBrnchCde() {
		return accntBnkBrnchCde;
	}

	public void setAccntBnkBrnchCde(String accntBnkBrnchCde) {
		this.accntBnkBrnchCde = accntBnkBrnchCde;
	}

	public String getAccntBnkBrnchBic() {
		return accntBnkBrnchBic;
	}

	public void setAccntBnkBrnchBic(String accntBnkBrnchBic) {
		this.accntBnkBrnchBic = accntBnkBrnchBic;
	}

	public String getAddrLine1() {
		return addrLine1;
	}

	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}

	public String getAddrLine2() {
		return addrLine2;
	}

	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}

	public String getRiskRating() {
		return riskRating;
	}

	public void setRiskRating(String riskRating) {
		this.riskRating = riskRating;
	}

	public Date getRegDt() {
		return regDt;
	}

	public void setRegDt(Date regDt) {
		this.regDt = regDt;
	}

	public Date getProfUptDt() {
		return profUptDt;
	}

	public void setProfUptDt(Date profUptDt) {
		this.profUptDt = profUptDt;
	}

	public Set<BorrowerRequest> getBorrowerReqList() {
		return borrowerReqList;
	}

	public void setBorrowerReqList(Set<BorrowerRequest> borrowerReqList) {
		this.borrowerReqList = borrowerReqList;
	}

	public Set<CurrentTransaction> getLenderTransList() {
		return lenderTransList;
	}

	public void setLenderTransList(Set<CurrentTransaction> lenderTransList) {
		this.lenderTransList = lenderTransList;
	}

	public Set<CurrentTransaction> getBorrowerTransList() {
		return borrowerTransList;
	}

	public void setBorrowerTransList(Set<CurrentTransaction> borrowerTransList) {
		this.borrowerTransList = borrowerTransList;
	}

	public Set<SuggestionDetails> getLenderSuggList() {
		return lenderSuggList;
	}

	public void setLenderSuggList(Set<SuggestionDetails> lenderSuggList) {
		this.lenderSuggList = lenderSuggList;
	}

	public Set<UserCreds> getUserList() {
		return userList;
	}

	public void setUserList(Set<UserCreds> userList) {
		this.userList = userList;
	}



    
}
