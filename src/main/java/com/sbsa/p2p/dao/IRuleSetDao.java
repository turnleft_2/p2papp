package com.sbsa.p2p.dao;

import com.sbsa.p2p.domain.RuleSet;
import com.sbsa.p2p.service.BO.RuleSetBO;

public interface IRuleSetDao {
	public RuleSet getRuleSet(double amount, double interestRate, String riskRating);
	public boolean initRuleSet(RuleSetBO reqBO);

}
