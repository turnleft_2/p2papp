package com.sbsa.p2p.dao;

public interface IInitDBDao {

	public boolean initDB();

}
