package com.sbsa.p2p.dao;

import com.sbsa.p2p.domain.Suggestions;
import com.sbsa.p2p.service.BO.BorrowerRequestBO;

public interface ISuggestionDao {
	
	public Suggestions getLoanSuggestion(BorrowerRequestBO requestBO);

}
