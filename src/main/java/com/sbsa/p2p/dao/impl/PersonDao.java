package com.sbsa.p2p.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IPersonDao;
import com.sbsa.p2p.domain.Person;
import com.sbsa.p2p.domain.UserCreds;
import com.sbsa.p2p.service.BO.LoginBO;
import com.sbsa.p2p.service.BO.LoginResultBO;
import com.sbsa.p2p.service.BO.PersonBO;
import com.sbsa.p2p.service.BO.RegisterRequestBO;


@Repository
@Transactional
public class PersonDao implements IPersonDao {
	
	
	@Autowired
	private SessionFactory sessionFactory;
	
	LoggerServiceImpl log = new LoggerServiceImpl(PersonDao.class);

	

	@Override
	public int getPersonCount()  {
		Session session = null;
		int count =0;
		
		try {
				session = sessionFactory.getCurrentSession();
				String hql  = " from Person ";	
			    Query query = session.createQuery(hql);			    
		       	
			    count = ((List<Person>)query.list()).size();	
		}catch (Exception ex){
			log.logError("Failed to get the count of persons with error", ex);
		}
		
		return count;
			  
	}



	@Override
	public PersonBO registerPerson(RegisterRequestBO reqBO) {
		
		Session session = null;
		 long personId =0L;
		 PersonBO personBo = null;
		
		try {
				session = sessionFactory.getCurrentSession();
				Person person = new Person();
				person.setFirstNm(reqBO.getFirstNm());
				person.setLastNm(reqBO.getLastNm());
				person.setUniqID(reqBO.getUniqId());
				person.setUniqIdName(reqBO.getUniqIdTyp());
				person.setPersonTyp(reqBO.getPersonTyp());
				person.setAccntBnkNm(reqBO.getAccntBnkNm());
				person.setAccntBnkBrnchBic(reqBO.getAccntBankBic());
				person.setAccntBnkBrnchCde(reqBO.getAccntBnkBrnchCde());
				person.setPhoneNum(reqBO.getPhoneNum());
				person.setAddrLine1(reqBO.getAddressLine1());
				person.setAddrLine2(reqBO.getAddressLine2());
				
				session.save(person);
				personBo = new PersonBO();
				personBo.setFirstNm(person.getFirstNm());
				personBo.setLastNm(person.getLastNm());
				personBo.setPersonId(person.getPersonId());
				personBo.setRegDt(person.getRegDt());
				personBo.setRiksRating(person.getRiskRating());
				
				
		}catch (Exception ex){
			log.logError("Failed to get create a new person record with error "+ ex.getMessage(), ex);
			
		}
		
		return personBo;
	}

	@Override
	public LoginResultBO authenticate(LoginBO reqBO) {
		 Session session = null;
		 PersonBO personBo = null;
		 UserCreds userCred = null;
		 String msg="";
		 LoginResultBO loginResultBO = new LoginResultBO();
		
		try {
			
			session = sessionFactory.getCurrentSession();
			String hql  = " from UserCreds where userName = :userName";	
			
			log.logInfo("Details ----->"+reqBO.getLoginName());
			Query query = session.createQuery(hql);			    
			query.setParameter("userName", reqBO.getLoginName());

			List<UserCreds> userList = (List<UserCreds>)query.list();		
			    if(userList.size() != 0){
			    	 userCred = userList.get(0);
			    	 loginResultBO.setLoginNm(userCred.getUserName());
			    	 loginResultBO.setErrorMsg("");
			    	 loginResultBO.setPersonId(userCred.getUser().getPersonId());
			    } else {
			    	log.logError("User"+reqBO.getLoginName()+" not in the database ");
			    	 loginResultBO.setLoginNm(reqBO.getLoginName());
			    	 loginResultBO.setErrorMsg("User"+reqBO.getLoginName()+" not in the database ");
			    }
			    

				
		}catch (Exception ex){
			log.logError("Failed to get create a new person record with error "+ ex.getMessage(), ex);
			 loginResultBO.setLoginNm(reqBO.getLoginName());
	    	 loginResultBO.setErrorMsg("User"+reqBO.getLoginName()+" failed with error:: "+ex.getMessage());
		}
		
		 return loginResultBO;
	}
	
	@Override
	public PersonBO registerPerson(RegisterRequestBO reqBO, long userCredId) {
		
		Session session = null;
		 long personId =0L;
		 PersonBO personBo = null;
		
		try {
				session = sessionFactory.getCurrentSession();
				Person person = new Person();
				person.setFirstNm(reqBO.getFirstNm());
				person.setLastNm(reqBO.getLastNm());
				person.setUniqID(reqBO.getUniqId());
				person.setUniqIdName(reqBO.getUniqIdTyp());
				person.setPersonTyp(reqBO.getPersonTyp());
				person.setAccntBnkNm(reqBO.getAccntBnkNm());
				person.setAccntBnkBrnchBic(reqBO.getAccntBankBic());
				person.setAccntBnkBrnchCde(reqBO.getAccntBnkBrnchCde());
				person.setPhoneNum(reqBO.getPhoneNum());
				person.setAddrLine1(reqBO.getAddressLine1());
				person.setAddrLine2(reqBO.getAddressLine2());				
				
				session.save(person);

				String hql  = " from UserCreds where userCredId=:userCredId ";	
			    Query query = session.createQuery(hql);	
			    query.setParameter("userCredId", userCredId);
			    
			   List<UserCreds> userCredList = (List<UserCreds>) query.list();
			    UserCreds userCred;
			    if(userCredList.size() == 1) {
			    	userCred = userCredList.get(0);
			    	userCred.setUser(person);

			    }
			    
				personBo = new PersonBO();
				personBo.setFirstNm(person.getFirstNm());
				personBo.setLastNm(person.getLastNm());
				personBo.setPersonId(person.getPersonId());
				personBo.setRegDt(person.getRegDt());
				personBo.setRiksRating(person.getRiskRating());
				
				
		}catch (Exception ex){
			log.logError("Failed to get create a new person record with error "+ ex.getMessage(), ex);
			
		}
		
		return personBo;
	}



	@Override
	public Person getPersonById(long personId) {
		Session session = null;
		Person person = null;
		try {
			session = sessionFactory.getCurrentSession();			
			String hql  = " from Person where personId = :personId";	
			Query query = session.createQuery(hql);
			query.setParameter("personId", personId);
			
			 List<Person> personList = (List<Person>)query.list();
			 if(personList != null && !personList.isEmpty() && personList.size()>0){
				 person = personList.get(0);
			 }
		    
		}catch (Exception ex){
			log.logError("Failed to get the count of persons with error", ex);
		}
		
		return person;
	}

	
}
