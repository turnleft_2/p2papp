package com.sbsa.p2p.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.ILenderProfileDao;
import com.sbsa.p2p.dao.IPersonDao;
import com.sbsa.p2p.dao.IRuleSetDao;
import com.sbsa.p2p.dao.ISuggestionDao;
import com.sbsa.p2p.domain.LenderProfile;
import com.sbsa.p2p.domain.Person;
import com.sbsa.p2p.domain.RuleSet;
import com.sbsa.p2p.domain.SuggestionDetails;
import com.sbsa.p2p.domain.Suggestions;
import com.sbsa.p2p.service.BO.BorrowerRequestBO;
import com.sbsa.p2p.service.BO.SuggestionBO;

@Repository
@Transactional
public class SuggestionDao implements ISuggestionDao {
	

	@Autowired
	private SessionFactory sessionFactory;
	
	LoggerServiceImpl log = new LoggerServiceImpl(SuggestionDao.class);
	@Autowired 
	private IPersonDao personDao;
	
	@Autowired
	IRuleSetDao ruleSetDao;
	
	@Autowired
	ILenderProfileDao profileDao;
	


	@Override
	public Suggestions getLoanSuggestion(BorrowerRequestBO requestBO) {
	
		Session session = null;
		Suggestions suggestion =null;		
		if(null != requestBO){
		try {
			session = sessionFactory.getCurrentSession();
			
			//get rating for Borrower
			Person person = personDao.getPersonById(requestBO.getPersonId());
			
			if(null != person){
				log.logInfo("=======================person" + person.getRiskRating());
				log.logInfo("=======================personid" + person.getPersonId());
				
				//Get the appropriate Rule
				//RuleSet ruleSet = getRuleSet(requestBO,person.getRiskRating()); 
				RuleSet ruleSet = getRuleSet(requestBO,"Medium");
			
				//get  matching Lender list
				List<LenderProfile> lenderProfiles = getLenderProfiles(ruleSet,requestBO);
			log.logInfo("before suggestion list");
				//prepare suggestion list
				if(null != lenderProfiles && lenderProfiles.size() >= ruleSet.getLenderCount()){
					double lendfromLender =  requestBO.getAmountToBorrow()/ruleSet.getLenderCount();
					double amountAlloted = 0;
					
					List<SuggestionDetails> suggestionDetails = new ArrayList<SuggestionDetails>();
					suggestion = new Suggestions();
					suggestion.setAmount(requestBO.getAmountToBorrow());
					suggestion.setInterestRate(requestBO.getRateRequested());
					suggestion.setTenure(requestBO.getTenureRequested());
					session.save(suggestion);
					for(int lenderCount = 0; lenderCount < lenderProfiles.size(); lenderCount++){
						LenderProfile lenderProfile = new LenderProfile();
						lenderProfile = lenderProfiles.get(lenderCount) ;
						SuggestionDetails details = new SuggestionDetails();
						details.setAmount(lendfromLender);
						details.setLenderSugg(lenderProfile.getPerson());
						details.setSuggestions(suggestion);
						session.save(details);
						
						log.logInfo("##***************" + suggestion.getSuggId());
						if(lenderCount == ruleSet.getLenderCount()) break;
					}
					
				}
				else{
					log.logInfo("---not matching--");
					return null;
				}
			}			
			
		}catch (Exception ex){
			log.logError("Failed to get Loan suggestion list "+ ex.getMessage(), ex);
			return null;
		}
		}
		log.logInfo("****************" + suggestion.getSuggId());
		return suggestion;
	}


	private List<LenderProfile> getLenderProfiles(RuleSet ruleSet,BorrowerRequestBO requestBO) {
		
		return profileDao.getLenderProfiles(ruleSet, requestBO);
	}

	private RuleSet getRuleSet(BorrowerRequestBO requestBO, String riskRating) {
		
		return ruleSetDao.getRuleSet(requestBO.getAmountToBorrow(),requestBO.getRateRequested(),riskRating);
		
	}
}
