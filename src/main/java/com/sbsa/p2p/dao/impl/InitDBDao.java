package com.sbsa.p2p.dao.impl;

import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IInitDBDao;
import com.sbsa.p2p.domain.BorrowerRequest;
import com.sbsa.p2p.domain.LenderProfile;
import com.sbsa.p2p.domain.Person;
import com.sbsa.p2p.domain.RuleSet;
import com.sbsa.p2p.service.BO.PersonBO;

@Repository
@Transactional
public class InitDBDao implements IInitDBDao {


	@Autowired
	private SessionFactory sessionFactory;
	
	LoggerServiceImpl log = new LoggerServiceImpl(PersonDao.class);
	@Override
	public boolean initDB() {
		Session session = null;
		 long personId =0L;
		 PersonBO personBo = null;
		
		 String [] name1 ={"Jay","Donald","Jack","Sam","Kay"};
		  String [] name2 ={"Jim","Paul","Jhon","Sammy","Kyle"};
		 
		try {
				session = sessionFactory.getCurrentSession();
				for(int i=0; i< 5 ; i++){
				Person person = new Person();
				person.setFirstNm(name1[i]);
				person.setLastNm("Tim");
				person.setUniqID("F1234567");
				person.setUniqIdName("Passport");
				person.setPersonTyp("B");
				person.setAccntBnkNm("1234567890");
				person.setAccntBnkBrnchBic("12345");
				person.setAccntBnkBrnchCde("5 simmond");
				person.setPhoneNum("987654321");
				person.setAddrLine1("address 1");
				person.setAddrLine2("address 2");
				
				BorrowerRequest borrowerRequest = new BorrowerRequest();
				borrowerRequest.setPerson(person);
				borrowerRequest.setBorrowAmt(1000);
				borrowerRequest.setRateRequested(3);
				borrowerRequest.setStatus("Medium");
				borrowerRequest.setTenureRequested(30);
				
				}
				for(int i=0; i< 5 ; i++){
				Person person = new Person();
				person.setFirstNm(name2[i]);
				person.setLastNm("tom");
				person.setUniqID("L123456");
				person.setUniqIdName("Passport");
				person.setPersonTyp("L");
				person.setAccntBnkNm("1234567890");
				person.setAccntBnkBrnchBic("12345");
				person.setAccntBnkBrnchCde("5 simmond");
				person.setPhoneNum("987654321");
				person.setAddrLine1("address 1");
				person.setAddrLine2("address 2");
				
				session.save(person);
				
				session.save(person);
				LenderProfile lenderProfile = new LenderProfile();
				lenderProfile.setPerson(person);
				lenderProfile.setCurrentBalance(i*1200);
				lenderProfile.setIsActive("Active");
				lenderProfile.setLendAmt(i*1000);
				lenderProfile.setRateRequired(3);
				lenderProfile.setRiskRequired("Medium");
				lenderProfile.setSuggBal(i*1000);
				lenderProfile.setTenureRequired(30);
				session.save(lenderProfile);
				}
				for(int i=1; i< 15 ; i++){
				RuleSet ruleSet = new RuleSet();
				ruleSet.setMaxAmt(1000);
				ruleSet.setRating("Medium");
				ruleSet.setInterestRate(3);
				ruleSet.setBankFeeRate(0.5);				
					ruleSet.setLenderCount(1);

				session.save(ruleSet);
				}
		}catch (Exception ex){
			log.logError("Failed to get create a new person record with error "+ ex.getMessage(), ex);
			
		}
		return true;
	}

}
