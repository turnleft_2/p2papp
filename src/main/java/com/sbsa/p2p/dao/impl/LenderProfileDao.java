package com.sbsa.p2p.dao.impl;

import java.util.ArrayList;
import java.util.List;

import org.hibernate.Criteria;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.criterion.Criterion;
import org.hibernate.criterion.Restrictions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.ILenderProfileDao;
import com.sbsa.p2p.domain.LenderProfile;
import com.sbsa.p2p.domain.RuleSet;
import com.sbsa.p2p.service.BO.BorrowerRequestBO;

@Repository
@Transactional
public class LenderProfileDao implements ILenderProfileDao {
	
	@Autowired
	private SessionFactory sessionFactory;
	
	LoggerServiceImpl log = new LoggerServiceImpl(LenderProfileDao.class);


	@Override
	public List<LenderProfile> getLenderProfiles(RuleSet ruleSet,BorrowerRequestBO requestBO) {
		List<LenderProfile> lenderProfiles = new ArrayList<LenderProfile>();
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();			

			Criteria crit = sessionFactory.getCurrentSession().createCriteria(LenderProfile.class);
			List<Criterion> criteriaToApply = new ArrayList<Criterion>();
			log.logInfo("HEELO get max amount"+ruleSet.getMaxAmt());
			log.logInfo("HEELO lender count ="+ruleSet.getLenderCount());
			log.logInfo("HEELO"+(double)(ruleSet.getMaxAmt()/ruleSet.getLenderCount()));
			criteriaToApply.add(Restrictions.le("lendAmt", (double)(ruleSet.getMaxAmt()/ruleSet.getLenderCount())));
			criteriaToApply.add(Restrictions.eq("rateRequired", ruleSet.getInterestRate()));
			criteriaToApply.add(Restrictions.eq("tenureRequired", requestBO.getTenureRequested()));
			criteriaToApply.add(Restrictions.eq("riskRequired", ruleSet.getRating()));
			
			if (criteriaToApply.size() == 1)
				crit.add(criteriaToApply.get(0));
			else if (criteriaToApply.size() > 1) {
				
				Criterion currentCriterion, nextCriterion;

				log.logInfo("1HEELO get max amount"+ruleSet.getMaxAmt());
				log.logInfo("1HEELO lender count ="+ruleSet.getLenderCount());
				log.logInfo("1HEELO"+(double)(ruleSet.getMaxAmt()/ruleSet.getLenderCount()));
				log.logInfo("*###########*adding criteria " + criteriaToApply.size());
				for (int i = 0; i < criteriaToApply.size(); i++) {
					log.logInfo("*###########*adding criteria count now  " + i);
					
					currentCriterion = criteriaToApply.get(i);
					i = i + 1;
					if (i< criteriaToApply.size()){
						nextCriterion = criteriaToApply.get(i);
						if (nextCriterion == null)
							crit.add(Restrictions.and(currentCriterion));
						else
							crit.add(Restrictions.and(currentCriterion, nextCriterion));
					} else {
						crit.add(Restrictions.and(currentCriterion));
					}
				}

			}
			log.logInfo("***count of records returned " + crit.list().size());
			lenderProfiles = (List<LenderProfile>) crit.list();
			log.logInfo("***count of records returned " + crit.list().size());

			
		}catch (Exception ex){
			log.logError("Failed to get the rulesSet", ex);
		}
		return lenderProfiles;
	}

}
