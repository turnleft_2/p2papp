package com.sbsa.p2p.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IRuleSetDao;
import com.sbsa.p2p.domain.RuleSet;
import com.sbsa.p2p.service.BO.RuleSetBO;

@Repository
@Transactional
public class RuleSetDao implements IRuleSetDao {
	@Autowired
	private SessionFactory sessionFactory;
	
	LoggerServiceImpl log = new LoggerServiceImpl(PersonDao.class);

	@Override
	public RuleSet getRuleSet(double amount, double interestRate, String riskRating){
		Session session = null;
		RuleSet ruleSet = new RuleSet();
		try {
			session = sessionFactory.getCurrentSession();
			
			String hql  = " from RuleSet where maxAmt = :amount and interestRate = :interestRate and riskRating = :riskRating";	

			Query query = session.createQuery(hql);		
			
			log.logInfo("Amount"+amount);
			log.logInfo("Amount"+interestRate);
			log.logInfo("Amount"+riskRating);

			query.setParameter("amount", amount);
			query.setParameter("interestRate", interestRate);
			query.setParameter("riskRating", riskRating);
			
			 List<RuleSet> reqQueueList = (List<RuleSet>)query.list();
			 if(reqQueueList != null && !reqQueueList.isEmpty() && reqQueueList.size()>0){
				 ruleSet = (RuleSet) reqQueueList.get(0);
			 }
		}catch (Exception ex){
			log.logError("Failed to get the rulesSet", ex);
		}
		return ruleSet;
	}

	@Override
	public boolean initRuleSet(RuleSetBO reqBO) {
		Session session = null;
		try {
			session = sessionFactory.getCurrentSession();		
			RuleSet ruleSet = new RuleSet();
			ruleSet.setMaxAmt(reqBO.getMaxAmt());
			ruleSet.setRating(reqBO.getRating());
			ruleSet.setInterestRate(reqBO.getInterestRate());
			ruleSet.setBankFeeRate(reqBO.getBankFeeRate());
			ruleSet.setLenderCount(reqBO.getLenderCount());
		
			session.save(ruleSet);
		}catch (Exception ex){
			log.logError("Failed to get create a new person record with error "+ ex.getMessage(), ex);
			return false;
		}
		
		return true;
	}

}
