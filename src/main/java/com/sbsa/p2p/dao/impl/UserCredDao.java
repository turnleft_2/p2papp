package com.sbsa.p2p.dao.impl;

import java.util.List;

import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IUserCredDao;
import com.sbsa.p2p.domain.UserCreds;
import com.sbsa.p2p.service.impl.RegistrationService;


@Repository
@Transactional
public class UserCredDao implements IUserCredDao {

	@Autowired
	private SessionFactory sessionFactory;
	
	LoggerServiceImpl log = new LoggerServiceImpl(UserCredDao.class);
	
	@Override
	public long checkAndcreateUser(String loginNm, String loginCred) {
		
		
		Session session = null;
		int count =0;
		
		try {
				session = sessionFactory.getCurrentSession();
				String hql  = " from UserCreds where userName=:userName ";	
			    Query query = session.createQuery(hql);	
			    query.setParameter("userName", loginNm);
			    
			    List<UserCreds>userCredList = ((List<UserCreds>)query.list());
			    if ((null == userCredList)|| (userCredList.size() == 0)){
			    	UserCreds userCred = new UserCreds();
			    	userCred.setUserName(loginNm);
			    	userCred.setUserCred(loginCred);
			    	session.save(userCred);
			    	
			    	return userCred.getUserCredId();
			    	
			    } else { 
			    	log.logInfo("UserDao already exists dont register again");
			    	return 0L;
			    }

			} catch (Exception ex){
				log.logError("Failed to create the user credential user ", ex);
				return -1L;
			}
	}
}