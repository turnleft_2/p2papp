package com.sbsa.p2p.dao;

import com.sbsa.p2p.domain.Person;
import com.sbsa.p2p.service.BO.LoginBO;
import com.sbsa.p2p.service.BO.LoginResultBO;
import com.sbsa.p2p.service.BO.PersonBO;
import com.sbsa.p2p.service.BO.RegisterRequestBO;


public interface IPersonDao {

	public int getPersonCount() ;

	public PersonBO registerPerson(RegisterRequestBO reqBO);
	
	public Person getPersonById(long personId);

	LoginResultBO authenticate(LoginBO reqBO);

	PersonBO registerPerson(RegisterRequestBO reqBO, long userCredId);

}
	




	
