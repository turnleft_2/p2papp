package com.sbsa.p2p.dao;

import java.util.List;

import com.sbsa.p2p.domain.LenderProfile;
import com.sbsa.p2p.domain.RuleSet;
import com.sbsa.p2p.service.BO.BorrowerRequestBO;

public interface ILenderProfileDao {
	public List<LenderProfile> getLenderProfiles(RuleSet ruleSet,BorrowerRequestBO requestBO);

}
