package com.sbsa.p2p.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.domain.Suggestions;
import com.sbsa.p2p.service.IInitDBService;
import com.sbsa.p2p.service.ILoanRequestService;
import com.sbsa.p2p.service.IRegistrationService;
import com.sbsa.p2p.service.IRuleSetService;
import com.sbsa.p2p.service.BO.BorrowerRequestBO;
import com.sbsa.p2p.service.BO.PersonBO;
import com.sbsa.p2p.service.BO.RegisterRequestBO;
import com.sbsa.p2p.service.BO.RuleSetBO;
import com.sbsa.p2p.service.BO.SuggestionBO;

@Controller
@RequestMapping(value = "service")
public class ExecController {
	
	
	LoggerServiceImpl log = new LoggerServiceImpl(this.getClass());
	
	
	@Autowired
	IRegistrationService registrationService;
	
	@RequestMapping(value = "/register", method = RequestMethod.POST)
	public @ResponseBody
	PersonBO updateExecQueue(@RequestBody RegisterRequestBO reqBO) throws Exception {

		log.logInfo("Processing for request  " + reqBO.toString());
	
		return registrationService.registerService(reqBO);
	}
	
	@Autowired
	IRuleSetService ruleSetService;
	
	@RequestMapping(value = "/initRuleSet", method = RequestMethod.POST)
	public @ResponseBody
	 boolean initialiseRuleSet(@RequestBody RuleSetBO reqBO) throws Exception {
		log.logInfo("Processing for request  " + reqBO.toString());
		return ruleSetService.initRuleSet(reqBO);
	}
	
	@Autowired
	IInitDBService initDBService;
	
	@RequestMapping(value = "/initDB", method = RequestMethod.GET)
	public @ResponseBody
	 boolean initDB() throws Exception {
		log.logInfo("Processing for request  ");
		return initDBService.initDB();
	}
	@Autowired
	ILoanRequestService loanRequestService;
	
	@RequestMapping(value = "/requestLoan", method = RequestMethod.POST)
	public @ResponseBody
	SuggestionBO requestLoan(@RequestBody BorrowerRequestBO reqBO) throws Exception {
		log.logInfo("Processing for request  " + reqBO.toString());
		return loanRequestService.getLoanSuggestion(reqBO);
	}
	
}
