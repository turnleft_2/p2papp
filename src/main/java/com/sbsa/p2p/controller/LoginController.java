package com.sbsa.p2p.controller;

import java.security.Principal;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseBody;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.service.ILoginService;
import com.sbsa.p2p.service.BO.LoginBO;
import com.sbsa.p2p.service.BO.LoginResultBO;

@Controller
public class LoginController {

		
		LoggerServiceImpl log = new LoggerServiceImpl(LoginController.class);
		
	    @Autowired
		ILoginService loginService;
	    
	    @RequestMapping(value="/homePage", method = RequestMethod.GET)
		public String showHome(ModelMap model, Principal principal ) {
			return "home";
		}
	    

		
		@RequestMapping(value = "/login", method = RequestMethod.POST)
		public @ResponseBody
		LoginResultBO loginUser(@RequestBody LoginBO reqBO) throws Exception {

			log.logInfo("Processing for request  " + reqBO.toString());
		
			return loginService.login(reqBO);
		}
	    
		
	

}