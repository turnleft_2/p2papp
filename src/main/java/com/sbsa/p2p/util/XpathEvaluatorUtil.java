package com.sbsa.p2p.util;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.List;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerException;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.w3c.dom.Text;
import org.xml.sax.InputSource;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;

public class XpathEvaluatorUtil {
	
	static LoggerServiceImpl log = new LoggerServiceImpl(XpathEvaluatorUtil.class);
	
	 public static synchronized  List<String> evaluateXpath(String expectedRespStr, String curRespStr, String xpathData) throws IOException, TransformerException  {
		 
		 
		 String[] xpathList =   xpathData.split(",");
		 ArrayList <String> diffResults = new ArrayList<String>();
		 
		 DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		 //TODO :: add namespace context functionality to enable namespace aware search.
		 //factory.setNamespaceAware(true); 
		 Document currRespDoc = null;
		 Document expectedRespDoc = null;
		try {

			expectedRespDoc =   factory.newDocumentBuilder().parse(new ByteArrayInputStream(expectedRespStr.getBytes()));
			currRespDoc =   factory.newDocumentBuilder().parse(new ByteArrayInputStream(curRespStr.getBytes()));
		} catch (Exception e) {
			log.logError("Failed to parse the document with error"+e.getMessage());
			return null ;
		}
				 
		 XPathFactory xPathfactory = XPathFactory.newInstance();
		 
		 ArrayList<String> failedList = new ArrayList<String>();
	     Node node = null;
		 
	     //evaluate expression result on XML document
		 String expectedData ="",currData="";
		 String expectedDefaultData ="";


		 for ( String xpathExpr : xpathList )
		 {
			 expectedDefaultData = "";
			 log.logInfo("Evaluating string "+xpathExpr.toString());
			 //System.out.println("Xpath ==="+xpathExpr.toString());
			 int indx = xpathExpr.toString().indexOf("=");
			 if(indx != -1){
				 expectedDefaultData = xpathExpr.toString().substring(indx+1);
				 xpathExpr = xpathExpr.substring(0,indx);
				 //System.out.println("String data "+expectedDefaultData);
			 }
			 
			try {
					XPathExpression xPathExpression=XPathFactory.newInstance().newXPath().compile(xpathExpr.toString());
				
					//evaluate expression result on XML document
					currData = (String) xPathExpression.evaluate(currRespDoc, XPathConstants.STRING);
					if(expectedDefaultData.isEmpty()){
						expectedData =(String) xPathExpression.evaluate(expectedRespDoc, XPathConstants.STRING);
					} else {
						expectedData = expectedDefaultData;
					}
					
					System.out.println("Expected data "+expectedData);
					System.out.println("Current Data "+ currData);
	            	
			} catch (XPathExpressionException e) {
				log.logError("Failed to extract node for xpath"+xpathExpr.toString(),e);
				return null;
			}
			
			if(!currData.equals(expectedData)){
				 diffResults.add("Expected Result for expression ["+xpathExpr.toString()+"] is ["+expectedData+"] but the current value is ["+currData+"]");
			}
		 }
		return diffResults;

	 }



	  public static void main (String args[]) throws IOException, TransformerException{
/*		  String xpathData = "/Envelope/Body/addResponse/return=25,/Envelope/Body/addResponse/return2";
		  String curRespStr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:c=\"http://c.b.a\"> <soapenv:Header/> <soapenv:Body> <c:addResponse> <c:return>25</c:return><c:return2>23</c:return2> </c:addResponse> </soapenv:Body> </soapenv:Envelope>";
		  String expectedRespStr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:c=\"http://c.b.a\"> <soapenv:Header/> <soapenv:Body> <c:addResponse> <c:return>25</c:return><c:return2>24</c:return2> </c:addResponse> </soapenv:Body> </soapenv:Envelope>";
		  List results =  evaluateXpath(expectedRespStr,curRespStr,xpathData);
		  System.out.println("Results =>"+results.toString());
*/		  
		  String xpathData = "/Envelope/Body/addResponse/return[text()=25]";
		  String curRespStr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:c=\"http://c.b.a\"> <soapenv:Header/> <soapenv:Body> <c:addResponse> <c:return>25</c:return><c:return>24</c:return> </c:addResponse> </soapenv:Body> </soapenv:Envelope>";
		  String expectedRespStr = "<soapenv:Envelope xmlns:soapenv=\"http://schemas.xmlsoap.org/soap/envelope/\" xmlns:c=\"http://c.b.a\"> <soapenv:Header/> <soapenv:Body> <c:addResponse> <c:return>24</c:return> </c:addResponse> </soapenv:Body> </soapenv:Envelope>";
		  boolean result = evalXPathExcluded(expectedRespStr,curRespStr,xpathData);
		  System.out.println("Results =>"+result);
		  
	  }
	  
	  public static void printDocument(Document doc, OutputStream out) throws IOException, TransformerException {
		    TransformerFactory tf = TransformerFactory.newInstance();
		    Transformer transformer = tf.newTransformer();
		    transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
		    transformer.setOutputProperty(OutputKeys.METHOD, "xml");
		    transformer.setOutputProperty(OutputKeys.INDENT, "yes");
		    transformer.setOutputProperty(OutputKeys.ENCODING, "UTF-8");
		    transformer.setOutputProperty("{http://xml.apache.org/xslt}indent-amount", "4");

		    transformer.transform(new DOMSource((org.w3c.dom.Node) doc), 
		         new StreamResult(new OutputStreamWriter(out, "UTF-8")));
		}

	  /**
	   * Removed elements from target xml that match with given xpaths
	   * Then compare target xml with given source xml.
	   * @param srcXml - xml to compare with (Ex. expected result xml).
	   * @param tarXml - xml to compare with given source xml (Ex. current result xml).
	   * @param xpaths - list of comma separated xpaths
	   * @return - true if target xml is same as source xml after removing elements matched by given xpaths.
	   */
	  public static synchronized boolean evalXPathExcluded(String srcXml, String tarXml, String xpaths) {
		// Create DOM object of source and target xmls.
		DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
		Document srcXmlDoc = null;
		Document tarXmlDoc = null;
		try {
			srcXmlDoc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(srcXml)));
			tarXmlDoc = factory.newDocumentBuilder().parse(new InputSource(new StringReader(tarXml)));
		} catch (Exception e) {
			log.logError("Document parsing error>" + e.getMessage());
			return false;
		}
		
		// Create XPath 
		XPath xPathEval = XPathFactory.newInstance().newXPath();

		// Get NodeList of matched xpaths from target xmls
		String[] xpathList = xpaths.split(",");
		ArrayList<NodeList> expRemoveList = new ArrayList<NodeList>();
		ArrayList<NodeList> removeList = new ArrayList<NodeList>();
		NodeList expNodeList,nodeList = null;
		for(String xPathVal: xpathList) {
			try {
				log.logInfo("xpath>>"+xPathVal);
				expNodeList = (NodeList) xPathEval.evaluate(xPathVal, srcXmlDoc, XPathConstants.NODESET);
				nodeList = (NodeList) xPathEval.evaluate(xPathVal, tarXmlDoc, XPathConstants.NODESET);
				log.logInfo("Remove nodeList.length>" + nodeList.getLength());
				expRemoveList.add(expNodeList);
				removeList.add(nodeList);
			} catch (XPathExpressionException e) {
				log.logError("Exception evaluating xpath>" + xPathVal + ", error>" + e.getMessage());
				return false;
			}
		}
		// Remove nodes matching with given xpath
		for(NodeList list: expRemoveList) {
			for(int i=0; i < list.getLength(); i++) {
				log.logInfo("Exp Remove node>>" + list.item(i)+"="+list.item(i).getTextContent());
				list.item(i).getParentNode().removeChild(list.item(i));
			}
		}
		for(NodeList list: removeList) {
			for(int i=0; i < list.getLength(); i++) {
				log.logInfo("Remove node>>" + list.item(i)+"="+list.item(i).getTextContent());
				list.item(i).getParentNode().removeChild(list.item(i));
			}
		}

		// Compare src and target xmls
		return compareNodes(srcXmlDoc, tarXmlDoc);
	  }
	  
	  /**
	   * Compares 2 XML Documents
	   * @param src - Document to be compared with
	   * @param trg - Document to compare
	   * @return - true if documents are identical
	   */
	private static boolean compareNodes(Node src, Node trg) {
		if (src.getNodeType() != trg.getNodeType()) {
			return false;
		}
		if (src.getNodeType() == Node.DOCUMENT_NODE) {
			if (!compareNodes(((Document) src).getDocumentElement(),
					((Document) trg).getDocumentElement())) {
				return false;
			}
		} else if (src.getNodeType() == Node.ELEMENT_NODE) {
			Element srcElement = (Element) src;
			Element trgElement = (Element) trg;

			// compare element names
			if (!srcElement.getTagName().equals(trgElement.getTagName())) {
				return false;
			}
			NodeList srcChildren = srcElement.getChildNodes();
			NodeList trgChildren = trgElement.getChildNodes();
			if (srcChildren.getLength() != trgChildren.getLength()) {
				return false;
			}
			for (int i = 0; i < srcChildren.getLength(); i++) {
				Node srcChild = srcChildren.item(i);
				Node trgChild = trgChildren.item(i);
				if (!compareNodes(srcChild, trgChild)) {
					return false;
				}
			}
		} else if (src instanceof Text) {
			String srcData = ((Text) src).getData().trim();
			String trgData = ((Text) trg).getData().trim();
			if (!srcData.equals(trgData)) {
				return false;
			}
		}
		return true;
	}
}
