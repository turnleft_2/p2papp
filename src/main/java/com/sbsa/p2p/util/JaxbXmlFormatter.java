package com.sbsa.p2p.util;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

import javax.xml.bind.annotation.adapters.XmlAdapter;

public class JaxbXmlFormatter extends XmlAdapter<String, Date> {
	private static SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
	
	@Override
	public String marshal(Date dt) {
		return sdf.format(dt);
	}
	
	@Override
	public Date unmarshal(String date) throws ParseException {
		return sdf.parse(date);
	}

	
}
