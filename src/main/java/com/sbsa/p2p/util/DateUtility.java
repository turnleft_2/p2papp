package com.sbsa.p2p.util;


import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import org.springframework.stereotype.Component;


@Component
public class DateUtility {
	private static SimpleDateFormat simpleDateFormat =  new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");

	public Date addorDeleteDays(int dayCount,Date incomingDate,boolean add){
		Calendar cal = Calendar.getInstance();
		cal.setTime(incomingDate);
		if (add) {
			cal.add(Calendar.DATE, +dayCount); //minus number would decrement the days
		}else{
			cal.add(Calendar.DATE,-dayCount); //minus number would decrement the days
		}
			return cal.getTime();
		}
	
	public String getFormattedDate(Date date) {
		return simpleDateFormat.format(date);
	}
	
	public Date getDate(String dateVal) throws ParseException {
		return simpleDateFormat.parse(dateVal);
	}

}
