package com.sbsa.p2p.util;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.xml.transform.TransformerException;
import javax.xml.xpath.XPathConstants;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;

public class SubStringEvaluatorUtil {
	
	
	
	public static void main (String args[])
	{
		
		  String subStringData  = "10:13,20:25=23456";
		  String curRespStr = "123456789012345689012345678901234567890";
		  String expectedRespStr = "121212212112331314232213141413123131123";
		  List results =  evaluateSubStringData(expectedRespStr,curRespStr,subStringData);
		  
		  System.out.println("Results =>"+results.toString());
	}

	public static synchronized  List evaluateSubStringData(String expectedRespStr,
			String curRespStr, String subStringData) {

		final LoggerServiceImpl log = new LoggerServiceImpl(SubStringEvaluatorUtil.class);
		 
			 String[] substringList =   subStringData.split(",");
			 ArrayList <String> diffResults = new ArrayList<String>();

			 
		     //evaluate expression result on XML document
			 String expectedData ="",currData="",expectedDefaultData="";
			 int startIndx =0 , endIndx =0;
			 String [] indexes ;
			 int currRespTotalLength = curRespStr.length();
			 int expectedRespTotalLength = expectedRespStr.length();
			 
			 if(currRespTotalLength != expectedRespTotalLength){
				 diffResults.add("Failed validations as the response data length["+currRespTotalLength+"] and expected "
				 		+ "response data length ["+expectedRespTotalLength+"] dont match ! ");
				 
				 return diffResults;
			 }

			 for ( String subString : substringList ) {
				 log.logInfo("Evaluating string "+subString);
				 
				 expectedDefaultData = "";
				
				try {
					
					int indx = subString.toString().indexOf("=");
					 if(indx != -1){
						 expectedDefaultData = subString.toString().substring(indx+1);
						 subString = subString.substring(0,indx);
						 //System.out.println("String data "+expectedDefaultData);
					 }
					
						indexes = subString.split(":");
						startIndx = Integer.valueOf(indexes[0]);
						endIndx = Integer.valueOf(indexes[1]);
					
						//evaluate expression result on XML document
						currData = curRespStr.substring(startIndx, endIndx);
						if(expectedDefaultData.isEmpty()){
							expectedData =expectedRespStr.substring(startIndx,endIndx);
						} else {
							expectedData = expectedDefaultData;
						}
		            	
				} catch (Exception e) {
					log.logError("Failed to extract substring value from data with error"+e.getMessage(),e);
					diffResults.add("Validate the substring value , failed to vaidate with error "+e.getMessage());
				}
				
				if(!currData.equalsIgnoreCase(expectedData)){
					 diffResults.add("Expected Result for sub string  ["+subString+"] is ["+expectedData+"] but the current value is ["+currData+"]");
				}
			 }
			return diffResults;
	}
}
