package com.sbsa.p2p.util;

import java.io.StringReader;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpression;
import javax.xml.xpath.XPathFactory;

import org.w3c.dom.Document;
import org.w3c.dom.NamedNodeMap;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.InputSource;

public class XMLUtil {

	

	public static synchronized Document loadXMLFromString(String xml) throws Exception
	{
	    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();
	    DocumentBuilder builder = factory.newDocumentBuilder();
	    factory.setNamespaceAware(true);
	    InputSource is = new InputSource(new StringReader(xml));
	    return builder.parse(is);
	}
	
	public static synchronized String getNodeValue(Document xmlDoc,String xpathString) throws Exception{
		   String data="";
		
		   XPathFactory xPathfactory = XPathFactory.newInstance();
		   XPath xpath = xPathfactory.newXPath();
		   XPathExpression expr = xpath.compile(xpathString);
		   NodeList nl = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
		   if (nl.getLength() ==1)
		   {
			   data=nl.item(0).getNodeValue();
		   }else {
			   data ="NOT FOUND";
		   }
		  
		   return data;
	 }
	
	public static synchronized String getAttribValue(Document xmlDoc,String xpathString) throws Exception{
		   String data="";
		
		   XPathFactory xPathfactory = XPathFactory.newInstance();
		   XPath xpath = xPathfactory.newXPath();
		   XPathExpression expr = xpath.compile(xpathString);
		   NodeList nl = (NodeList) expr.evaluate(xmlDoc, XPathConstants.NODESET);
		   
		   System.out.println("DATA"+nl.getLength());
		   
		   if (nl.getLength() ==1)
		   {
			  NamedNodeMap a = nl.item(0).getAttributes();
			  System.out.println("Map length =>"+a.getLength());
			  Node temp =null;
			  for(int i=0;i<a.getLength();i++){
				  temp = a.item(i);
				  if(temp.getNodeName().indexOf("value")!=-1)
				  {
					  data = temp.getNodeValue();
					  break;
				  }
			  }
			  
			 
		   }else {
			   data ="";
		   }
		  
		   return data;
	 }
	
	public static void main (String args [] ) throws Exception{
		String msg ="<wmb:event xmlns:wmb=\"http://www.ibm.com/xmlns/prod/websphere/messagebroker/6.1.0/monitoring/event\"> "
				+ " <wmb:eventPointData>   <wmb:eventData wmb:productVersion=\"8001\" wmb:eventSchemaVersion=\"6.1.0.3\" "
				+ "wmb:eventSourceAddress=\"File Input.terminal.out\">    <wmb:eventIdentity wmb:eventName=\"File Input.OutTerminal\"/> "
				+ "   <wmb:eventSequence wmb:creationTime=\"2015-06-09T08:16:25.449Z\" wmb:counter=\"1\"/>   "
				+ " <wmb:eventCorrelation wmb:localTransactionId=\"532f442b-bf88-449b-8187-55ee1e729afd-1\" wmb:parentTransactionId=\"\" "
				+ "wmb:globalTransactionId=\"\"/>   </wmb:eventData>   <wmb:messageFlowData>    <wmb:broker wmb:name=\"WMB8BRK\" wmb:UUID=\"2d1d2d36-d657-4881-bc3f-5016a4f8b04d\"/>    "
				+ "<wmb:executionGroup wmb:name=\"EG1\" wmb:UUID=\"d616c514-4a01-0000-0080-9a35c27a1b68\"/>  "
				+ "  <wmb:messageFlow wmb:uniqueFlowName=\"WMB8BRK.EG1.FileToFileSample\" wmb:name=\"FileToFileSample\" "
				+ "wmb:UUID=\"643d48d7-4d01-0000-0080-e7f83ecd37dc\" wmb:threadId=\"19884\"/>   "
				+ " <wmb:node wmb:nodeLabel=\"File Input\" wmb:nodeType=\"ComIbmFileInputNode\" wmb:terminal=\"out\"/>  "
				+ " </wmb:messageFlowData>  </wmb:eventPointData>  <wmb:applicationData xmlns=\"\">  "
				+ "<wmb:simpleContent wmb:name=\"Directory\""
				+ " wmb:value=\"C:\\WMB_File\\Data\\input\" "
				+ "wmb:dataType=\"string\"/>   "
				+ "<wmb:simpleContent wmb:name=\"Name\" wmb:value=\"data.input\" wmb:dataType=\"string\"/>  </wmb:applicationData> "
				+ " <wmb:bitstreamData>   <wmb:bitstream wmb:encoding=\"base64Binary\">PGRhdGE+PGNvbnRlbnQ+ZGF0YSBpbnB1dDwvY29udGVudD48L2RhdGE+DQo=</wmb:bitstream> "
				+ " </wmb:bitstreamData> </wmb:event>";
		Document xmlDoc = XMLUtil.loadXMLFromString(msg);
		xmlDoc.getDocumentElement().normalize();
		String data = XMLUtil.getAttribValue(xmlDoc, "/event/applicationData/simpleContent[@name='Directory']");
		
		System.out.println("Extracted Xpath for Directory =>"+data);
		
		data = XMLUtil.getAttribValue(xmlDoc, "/event/applicationData/simpleContent[@name='Name']");
		
		System.out.println("Extracted Xpath for File name =>"+data);
	}
	
}
