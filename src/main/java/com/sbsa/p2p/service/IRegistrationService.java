package com.sbsa.p2p.service;

import com.sbsa.p2p.service.BO.PersonBO;
import com.sbsa.p2p.service.BO.RegisterRequestBO;

public interface IRegistrationService {

	public PersonBO registerService(RegisterRequestBO reqBO);
	
}
