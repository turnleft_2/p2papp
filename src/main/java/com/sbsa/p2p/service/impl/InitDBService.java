package com.sbsa.p2p.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sbsa.p2p.dao.IInitDBDao;
import com.sbsa.p2p.dao.IRuleSetDao;
import com.sbsa.p2p.service.IInitDBService;
import com.sbsa.p2p.service.BO.RuleSetBO;

@Component
public class InitDBService implements IInitDBService {

	@Autowired
	IInitDBDao initDBDao;
	@Override
	public boolean initDB() {
		// TODO Auto-generated method stub
		return initDBDao.initDB();
	}
	
}
