
package com.sbsa.p2p.service.impl;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FilenameFilter;
import java.io.IOException;
import java.io.RandomAccessFile;
import java.util.HashMap;
import java.util.Properties;

import org.apache.commons.io.FilenameUtils;
import org.hibernate.service.config.spi.ConfigurationService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IPersonDao;
import com.sbsa.p2p.service.IPersonService;



@Component
public class PersonService implements IPersonService {
	LoggerServiceImpl log = new LoggerServiceImpl(ConfigurationService.class);
	
	@Autowired
	IPersonDao personDao;

	@Override
	public int getPersonsCount() {
		
		return personDao.getPersonCount();

	}
}
	
	
