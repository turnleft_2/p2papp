package com.sbsa.p2p.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IPersonDao;
import com.sbsa.p2p.service.ILoginService;
import com.sbsa.p2p.service.BO.LoginBO;
import com.sbsa.p2p.service.BO.LoginResultBO;

@Component
public class LoginService implements ILoginService {
	

	LoggerServiceImpl log = new LoggerServiceImpl(this.getClass());
	
	@Autowired
	IPersonDao personDao;

	@Override
	public LoginResultBO login(LoginBO reqBO) {
		
		return personDao.authenticate(reqBO);
	}
	
	
	

	
}
