package com.sbsa.p2p.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sbsa.p2p.dao.IRuleSetDao;
import com.sbsa.p2p.service.IRuleSetService;
import com.sbsa.p2p.service.BO.RuleSetBO;

@Component
public class RuleSetService implements IRuleSetService {

	@Autowired
	IRuleSetDao iRuleSetDao;
	
	@Override
	public boolean initRuleSet(RuleSetBO reqBO) {
		
		return iRuleSetDao.initRuleSet(reqBO);
	}

}
