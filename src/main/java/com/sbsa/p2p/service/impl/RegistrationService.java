package com.sbsa.p2p.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sbsa.p2p.common.logging.impl.LoggerServiceImpl;
import com.sbsa.p2p.dao.IPersonDao;
import com.sbsa.p2p.dao.IUserCredDao;
import com.sbsa.p2p.service.IRegistrationService;
import com.sbsa.p2p.service.BO.PersonBO;
import com.sbsa.p2p.service.BO.RegisterRequestBO;

@Component
public class RegistrationService implements IRegistrationService {

	LoggerServiceImpl log = new LoggerServiceImpl(RegistrationService.class);
	
	@Autowired	
	IPersonDao personDao;
	
	@Autowired
	IUserCredDao userCredDao;
	
	@Override
	public PersonBO registerService(RegisterRequestBO reqBO) {
		
		PersonBO personBO;
		
		//create the user in user cred table , check if he exists already if exists fail
		long userCredId = userCredDao.checkAndcreateUser(reqBO.getLoginNm(),reqBO.getLoginCred());
		if (userCredId != 0L){
			log.logInfo("Created the user login id ");
			//create person
			personBO = personDao.registerPerson(reqBO,userCredId);
			log.logInfo("Created the personBO "+personBO);
		} else {
			log.logInfo("User already exists ");
			
			personBO = new PersonBO();
			personBO.setErrMsg("User alerdy exists!");
		}
		return personBO;
	}

}
