package com.sbsa.p2p.service.impl;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.sbsa.p2p.dao.ISuggestionDao;
import com.sbsa.p2p.domain.Suggestions;
import com.sbsa.p2p.service.ILoanRequestService;
import com.sbsa.p2p.service.BO.BorrowerRequestBO;
import com.sbsa.p2p.service.BO.SuggestionBO;

@Component
public class LoanRequestService implements ILoanRequestService {
	
	@Autowired
	ISuggestionDao suggestionDao;

	@Override
	public SuggestionBO getLoanSuggestion(BorrowerRequestBO requestBO) {
		Suggestions suggestions = suggestionDao.getLoanSuggestion(requestBO);
		return convertDomainToBO(suggestions);
	}

	private SuggestionBO convertDomainToBO(Suggestions suggestions) {
		SuggestionBO suggestionBO = new SuggestionBO();
		
		if(null != suggestions){
			suggestionBO.setAmount(suggestions.getAmount());
			suggestionBO.setInterestRate(suggestions.getInterestRate());
			suggestionBO.setSuggId(suggestions.getSuggId());;
			suggestionBO.setTenure(suggestions.getTenure());
			suggestionBO.setErrorCode(0);
			suggestionBO.setErrorMsg("");
			
		}else{
			suggestionBO.setErrorCode(1);
			suggestionBO.setErrorMsg("There is no loan suggestion available for the request made");
		}
		return suggestionBO;
	}

}
