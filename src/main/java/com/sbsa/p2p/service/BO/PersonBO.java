package com.sbsa.p2p.service.BO;

import java.util.Date;

import org.codehaus.jackson.map.annotate.JsonDeserialize;
import org.codehaus.jackson.map.annotate.JsonSerialize;

import com.sbsa.p2p.util.JsonDateDeserializer;
import com.sbsa.p2p.util.JsonDateSerializer;

public class PersonBO {
	private long personId;
	private String firstNm ;
	private String lastNm;
	private String uniqId;
	private String uniqIdTyp;
	private String personTyp;
	private String phoneNum;
	private String accntNum;
	private String accBrnchNm;
	private String accBrnchCde;
	private String accBrnchBic;
	private String addrLine1;
	private String addrLine2;
	private Date regDt;
	private Date profLastUpt;
	private String riksRating;
	private String errMsg;

	public String getErrMsg() {
		return errMsg;
	}
	public void setErrMsg(String errMsg) {
		this.errMsg = errMsg;
	}
	public String getFirstNm() {
		return firstNm;
	}
	public void setFirstNm(String firstNm) {
		this.firstNm = firstNm;
	}
	public String getLastNm() {
		return lastNm;
	}
	public void setLastNm(String lastNm) {
		this.lastNm = lastNm;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public String getUniqId() {
		return uniqId;
	}
	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}
	public String getUniqIdTyp() {
		return uniqIdTyp;
	}
	public void setUniqIdTyp(String uniqIdTyp) {
		this.uniqIdTyp = uniqIdTyp;
	}
	public String getPersonTyp() {
		return personTyp;
	}
	public void setPersonTyp(String personTyp) {
		this.personTyp = personTyp;
	}
	public String getAccntNum() {
		return accntNum;
	}
	public void setAccntNum(String accntNum) {
		this.accntNum = accntNum;
	}
	public String getAccBrnchNm() {
		return accBrnchNm;
	}
	public void setAccBrnchNm(String accBrnchNm) {
		this.accBrnchNm = accBrnchNm;
	}
	public String getAccBrnchCde() {
		return accBrnchCde;
	}
	public void setAccBrnchCde(String accBrnchCde) {
		this.accBrnchCde = accBrnchCde;
	}
	public String getAccBrnchBic() {
		return accBrnchBic;
	}
	public void setAccBrnchBic(String accBrnchBic) {
		this.accBrnchBic = accBrnchBic;
	}
	public String getAddrLine1() {
		return addrLine1;
	}
	public void setAddrLine1(String addrLine1) {
		this.addrLine1 = addrLine1;
	}
	public String getAddrLine2() {
		return addrLine2;
	}
	public void setAddrLine2(String addrLine2) {
		this.addrLine2 = addrLine2;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)  
	public Date getRegDt() {
		return regDt;
	}
	
	@JsonDeserialize(using=JsonDateDeserializer.class) 
	public void setRegDt(Date regDt) {
		this.regDt = regDt;
	}
	
	@JsonSerialize(using=JsonDateSerializer.class)  
	public Date getProfLastUpt() {
		return profLastUpt;
	}
	
	@JsonDeserialize(using=JsonDateDeserializer.class) 
	public void setProfLastUpt(Date profLastUpt) {
		this.profLastUpt = profLastUpt;
	}
	public String getRiksRating() {
		return riksRating;
	}
	public void setRiksRating(String riksRating) {
		this.riksRating = riksRating;
	}
	
}
