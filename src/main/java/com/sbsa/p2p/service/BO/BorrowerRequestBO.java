package com.sbsa.p2p.service.BO;

public class BorrowerRequestBO {
	protected long personId;
	protected double amountToBorrow;
	protected double rateRequested;
	protected int tenureRequested;
	public long getPersonId() {
		return personId;
	}
	public void setPersonId(long personId) {
		this.personId = personId;
	}
	public double getAmountToBorrow() {
		return amountToBorrow;
	}
	public void setAmountToBorrow(double amountToBorrow) {
		this.amountToBorrow = amountToBorrow;
	}
	public double getRateRequested() {
		return rateRequested;
	}
	public void setRateRequested(double rateRequested) {
		this.rateRequested = rateRequested;
	}
	public int getTenureRequested() {
		return tenureRequested;
	}
	public void setTenureRequested(int tenureRequested) {
		this.tenureRequested = tenureRequested;
	}
	
}
