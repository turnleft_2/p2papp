package com.sbsa.p2p.service.BO;

public class RuleSetBO {
	
	private Long ruleSetId;
    private double maxAmt;
    private double interestRate;
    private double bankFeeRate;
    private int lenderCount;
    private String rating;
	public Long getRuleSetId() {
		return ruleSetId;
	}
	public void setRuleSetId(Long ruleSetId) {
		this.ruleSetId = ruleSetId;
	}
	public double getMaxAmt() {
		return maxAmt;
	}
	public void setMaxAmt(double maxAmt) {
		this.maxAmt = maxAmt;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public double getBankFeeRate() {
		return bankFeeRate;
	}
	public void setBankFeeRate(double bankFeeRate) {
		this.bankFeeRate = bankFeeRate;
	}
	public int getLenderCount() {
		return lenderCount;
	}
	public void setLenderCount(int lenderCount) {
		this.lenderCount = lenderCount;
	}
	public String getRating() {
		return rating;
	}
	public void setRating(String rating) {
		this.rating = rating;
	}

}
