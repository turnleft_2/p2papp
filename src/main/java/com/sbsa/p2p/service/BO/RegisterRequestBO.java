package com.sbsa.p2p.service.BO;

public class RegisterRequestBO {
	
	protected String loginNm;
	protected String loginCred;
	protected String firstNm;
	protected String lastNm;
	protected String uniqId;
	protected String uniqIdTyp;
	protected String personTyp;
	protected String accountNum;
	protected String accntBnkNm;
	protected String accntBnkBrnchCde;
	protected String accntBankBic;
	protected String phoneNum;
	protected String addressLine1;
	protected String addressLine2;
	
		
	public String getLoginNm() {
		return loginNm;
	}
	public void setLoginNm(String loginNm) {
		this.loginNm = loginNm;
	}
	public String getLoginCred() {
		return loginCred;
	}
	public void setLoginCred(String loginCred) {
		this.loginCred = loginCred;
	}
	public String getFirstNm() {
		return firstNm;
	}
	public void setFirstNm(String firstNm) {
		this.firstNm = firstNm;
	}
	public String getLastNm() {
		return lastNm;
	}
	public void setLastNm(String lastNm) {
		this.lastNm = lastNm;
	}
	public String getUniqId() {
		return uniqId;
	}
	public void setUniqId(String uniqId) {
		this.uniqId = uniqId;
	}
	public String getUniqIdTyp() {
		return uniqIdTyp;
	}
	public void setUniqIdTyp(String uniqIdTyp) {
		this.uniqIdTyp = uniqIdTyp;
	}
	public String getPersonTyp() {
		return personTyp;
	}
	public void setPersonTyp(String personTyp) {
		this.personTyp = personTyp;
	}
	public String getAccountNum() {
		return accountNum;
	}
	public void setAccountNum(String accountNum) {
		this.accountNum = accountNum;
	}
	
	public String getAccntBnkNm() {
		return accntBnkNm;
	}
	public void setAccntBnkNm(String accntBnkNm) {
		this.accntBnkNm = accntBnkNm;
	}
	public String getAccntBnkBrnchCde() {
		return accntBnkBrnchCde;
	}
	public void setAccntBnkBrnchCde(String accntBnkBrnchCde) {
		this.accntBnkBrnchCde = accntBnkBrnchCde;
	}
	public String getAccntBankBic() {
		return accntBankBic;
	}
	public void setAccntBankBic(String accntBankBic) {
		this.accntBankBic = accntBankBic;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getAddressLine1() {
		return addressLine1;
	}
	public void setAddressLine1(String addressLine1) {
		this.addressLine1 = addressLine1;
	}
	public String getAddressLine2() {
		return addressLine2;
	}
	public void setAddressLine2(String addressLine2) {
		this.addressLine2 = addressLine2;
	}
	
	
}
