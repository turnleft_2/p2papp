package com.sbsa.p2p.service.BO;

public class SuggestionBO {
	
	private Long suggId;
    private double amount;
    private double interestRate;
    private long tenure;
    private String errorMsg;
    private int errorCode;
    
	public Long getSuggId() {
		return suggId;
	}
	public void setSuggId(Long suggId) {
		this.suggId = suggId;
	}
	public double getAmount() {
		return amount;
	}
	public void setAmount(double amount) {
		this.amount = amount;
	}
	public double getInterestRate() {
		return interestRate;
	}
	public void setInterestRate(double interestRate) {
		this.interestRate = interestRate;
	}
	public long getTenure() {
		return tenure;
	}
	public void setTenure(long tenure) {
		this.tenure = tenure;
	}
	public String getErrorMsg() {
		return errorMsg;
	}
	public void setErrorMsg(String errorMsg) {
		this.errorMsg = errorMsg;
	}
	public int getErrorCode() {
		return errorCode;
	}
	public void setErrorCode(int errorCode) {
		this.errorCode = errorCode;
	}

}
