package com.sbsa.p2p.service.BO;

public class LoginBO {

	private String loginName ;
	private String loginCred;
	
	public String getLoginName() {
		return loginName;
	}
	public void setLoginName(String loginName) {
		this.loginName = loginName;
	}
	public String getLoginCred() {
		return loginCred;
	}
	public void setLoginCred(String loginCred) {
		this.loginCred = loginCred;
	}

	
	
}
