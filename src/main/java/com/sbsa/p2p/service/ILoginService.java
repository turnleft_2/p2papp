package com.sbsa.p2p.service;

import com.sbsa.p2p.service.BO.LoginBO;
import com.sbsa.p2p.service.BO.LoginResultBO;

public interface ILoginService {
	
	public LoginResultBO login(LoginBO reqBO);

}
