package com.sbsa.p2p.service;

import com.sbsa.p2p.service.BO.BorrowerRequestBO;
import com.sbsa.p2p.service.BO.SuggestionBO;

public interface ILoanRequestService {
	public SuggestionBO getLoanSuggestion(BorrowerRequestBO request);

}
