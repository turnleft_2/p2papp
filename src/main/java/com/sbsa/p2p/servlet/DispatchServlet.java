package com.sbsa.p2p.servlet;

import javax.servlet.ServletConfig;
import javax.servlet.ServletException;

import org.springframework.web.servlet.DispatcherServlet;

public class DispatchServlet extends DispatcherServlet {

	
    /**
     * Initializes the servlet.
     */
    public void init(ServletConfig config) throws ServletException {
        super.init(config);
        
        System.err.println("\n Peer2Peer Banking App  DispatchServlet is Initializing ...\n");

        try {
        	
        	//Do any intialization routines.
            
 
        } catch (Exception e) {
            logger.error("An Exception has occurred: " + e.getMessage(), e);
        }

        System.err.println("\n Peer2Peer Banking App init Completed.\n");
    }
}
