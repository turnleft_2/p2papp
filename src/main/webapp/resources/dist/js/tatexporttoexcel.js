//Export jqGrid data to excel
function exportGrid(gridId){
	var grid = $("#"+gridId);
	var cols = grid.jqGrid("getGridParam", "colNames");
	var html="<table border='2px'><tr>";
	for (var i=0; i<cols.length; i++) {
		html += "<th bgcolor='#FFFF00'>"+ cols[i] +"</th>";
	}
	html+="</tr>";
	var gids = grid.jqGrid("getDataIDs");
	var cns = grid.jqGrid("getGridParam", "colModel");
	var rData;
	var rclr;
	for (var ii=0; ii<gids.length;ii++) {
		rData = grid.jqGrid("getRowData", gids[ii]);
		html += "<tr>";
		if(ii%2 == 0) rclr = "";
		else rclr = "#E6E6FA";
		for (var i=0; i<cns.length; i++) {
			html += "<td bgcolor='"+rclr+"'>"+ rData[cns[i].name].replace(/&/g, "&amp;")
				.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/"/g, "&quot;")+"</td>";
		}
		html += "</tr>";
	}
	var dt = new Date();
	var day = dt.getDate();
	var month = dt.getMonth() + 1;
	var year = dt.getFullYear();
	var hour = dt.getHours();
	var mins = dt.getMinutes();
	var secs = dt.getSeconds();
	var fts = day + "-" + month + "-" + year + "_" + hour + "-" + mins + "-" +secs;
	var fname = "TATExportedData-" + fts + ".xls";
	var ie10nOlder = (navigator.userAgent.indexOf('MSIE') != -1); // IE10 or older
	var ie11nNewer = (navigator.userAgent.indexOf('Trident') != -1); // IE11 or newer
	console.log("Browser.ie11nNewer>"+ie11nNewer +", ie10nOlder>" +ie10nOlder);
	if (ie11nNewer || ie10nOlder) {
	
		if (ie10nOlder){ // IE10-
			// This opens up a save as dialog. Client side download option is not supported in older IE versions.
			var frame = $('<iframe id="downloadFile" style="display:none"/>').appendTo('body');
			frame.ready(function(){
				var frm = frame[0].contentWindow.document;
				frm.open("text/html", "replace");
				frm.write(html);
				frm.close();
				frm.execCommand('SaveAs', false, fname);
				frm.close();
			});
			$('#downloadFile').remove();
		} else if (ie11nNewer) { // IE11+
			if (window.navigator.msSaveOrOpenBlob) {
				navigator.msSaveOrOpenBlob(new Blob([html],{type: "text/html;charset=utf-8;"}), fname);
			}
		} 
	} else {	// Non IE browsers
		$('#downloadFile').remove();
		$('<a></a>')
		.attr('id','downloadFile')
		.attr('href','data:application/vnd.ms-excel;charset=utf-8,' + encodeURIComponent(html))
		.attr('download','TATExportedData-' + fname)
		.appendTo('body');
		$('#downloadFile').get(0).click();
	}
}

