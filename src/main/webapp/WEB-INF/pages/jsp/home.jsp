<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<link rel="shortcut icon" href="../../docs-assets/ico/favicon.png">
<title>Peer2PeerApp</title>
<!-- Bootstrap core CSS -->
<link href="resources/css/bootstrap.css" rel="stylesheet">
<link href="resources/css/custom.css" rel="stylesheet">
<!-- Navigation Bar Drop Down CSS -->
<link href="resources/css/dropdown.style.css" rel="stylesheet" />
<style>
#footer {
	height: 60px;
	background-color: #eee;
	margin-top: 130px;
	color: #555555;
	font-weight: bold;
	padding-top: 10px;
}

#foo {
	width: 10%;
	float: right;
}

#foot {
	padding: 1px 1px 1px 1px;
}

/* Lastly, apply responsive CSS fixes as necessary */
@media ( max-width : 767px) {
	#footer {
		margin-left: -20px;
		margin-right: -20px;
		margin-top: 20px;
		padding-left: 20px;
		padding-right: 20px;
	}
	h4 {
		font-size: 14px;
	}
	#foo {
		width: 100%;
		float: right;
	}
}
</style>
</head>
<body>
	<jsp:include page="header.jsp" />
	<div class="container">
		<div class="jumbotron">
			<h4>Welcome to Peer To Peer Banking Application</h4>
			<!-- Main content -->
			<div class="maincontent">
				<img class="img-responsive pull-left" alt="Standbic IBTC Bank"
					src="resources/images/landingpageImg.jpg">
				<p>Peer To Peer Banking Application allows  SBSA banking customers to lend their l
				liquid money to needy borrowers  that is beneficial to both parties.</p>
	
			</div>
		</div>
	</div>


	<!-- Footer -->
	<div class="container">
		<div id="footer">
			<div id="foo">
				<p class="muted credit">P2P-1.0.0.0</p>
			</div>
		</div>
	</div>

	<!-- Footer ends-->
	<!-- Bootstrap core JavaScript
    ================================================== -->

	<script src="resources/js/jquery-1.9.1.min.js"></script>
	<script src="resources/js/bootstrap.min.js"></script>


</body>
</html>
