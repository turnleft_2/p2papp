<%@ taglib prefix="sec"
	uri="http://www.springframework.org/security/tags"%>
<div class="container">
	<div>
		<img src="resources/images/sbsaLogo.jpg" alt="Standbic IBTC Bank"
			class="img-responsive" id="img_banner" />
	</div>
	<div>
		<ul class="nav nav-pills">
			<li id="homeNavOption"><a href="/TestAutoTool/homePage">Home</a></li>
			<li id="createTestBedOption"><a href="/TestAutoTool/createtesttemplate">CreateTestTemplate</a></li>
			<li id="searchNavOption"><a href="/TestAutoTool/execPage">ExecuteTestSuite</a></li>
			<li id="createTestBedOption"><a href="/TestAutoTool/createtestbed">ConfigureTestBed-ServiceRepo</a></li>
			<li id="createTestBedOption"><a href="/TestAutoTool/createsvntest">ConfigureTestBed-TestSuiteRepo</a></li>
			<li id="reportNavOption"><a href="/TestAutoTool/reportPage">Report</a></li>
			<li id="reportNavOption"><a href="/TestAutoTool/svnreport">TestSuite-Report</a></li>
				
			<!-- <li id="statusNavOption"><a href="/TestAutoTool/statusPage">Status</a></li> -->	
			<li class="pull-right settings" id="showHelpOption"><a href="/TestAutoTool/help/helpTarget=homePage" 
				onclick="helpClick(); return false;">Help?</a></li>
		</ul>
		
		<p class="pull-right settings"></p>
		<p class="pull-right settings">
			<span class="glyphicon glyphicon-wrench">&nbsp;MyProfile</span>
		</p>
	</div>
</div>
<!-- Script function called on click of Help option -->
<script type="text/javascript">
	function helpClick() {
		var pname = $(location).attr('pathname');
		var sname=pname.substring(pname.lastIndexOf("/")+1);
		var turl = "/TestAutoTool/help/"+sname;
		window.open(turl,'Test Automation Tool','scrollbars=yes,width=550,height=400,left=800,top=90');
	}
</script>
